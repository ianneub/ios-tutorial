//
//  AddToDoItemViewController.swift
//  ios-tutorial
//
//  Created by Ian Neubert on 9/13/14.
//  Copyright (c) 2014 Ian Neubert. All rights reserved.
//

import UIKit

class AddToDoItemViewController: UIViewController, UITextFieldDelegate {
    var toDoItem = ToDoItem(itemName: "")

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if sender as UIBarButtonItem == self.doneButton {
            if !self.textField.text.isEmpty {
                self.toDoItem.itemName = self.textField.text
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
