//
//  ToDoItem.swift
//  ios-tutorial
//
//  Created by Ian Neubert on 9/13/14.
//  Copyright (c) 2014 Ian Neubert. All rights reserved.
//

import UIKit

class ToDoItem {
    var itemName: String
    var completed: Bool = false
    var creationDate: NSDate
    
    init(itemName: String) {
        self.itemName = itemName
        self.creationDate = NSDate()
    }
}
